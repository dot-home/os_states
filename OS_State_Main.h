
//********************************************************************************
/*!
\author     Kraemer E.
\date       04.10.2020

\file       OS_STATE_Main.h
\brief      Main state of the OS. This have to be called every time an event
			is detected.
***********************************************************************************/
#ifndef _OS_STATE_MAIN_H_
#define _OS_STATE_MAIN_H_

#ifdef __cplusplus
extern "C"
{
#endif

/********************************* includes **********************************/
#include "BaseTypes.h"
#include "OS_EventManager.h"

/***************************** defines / macros ******************************/

/************************ externally visible functions ***********************/
u8 OS_State_Main(teEventID eEventID, uiEventParam1 uiParam1, ulEventParam2 ulParam2);

#ifdef __cplusplus
}
#endif

#endif // _OS_STATE_MAIN_H_

/* [] END OF FILE */
