


/********************************* includes **********************************/
#include "OS_State_Reset.h"
#include "OS_StateManager.h"
#include "OS_SoftwareTimer.h"
#include "OS_Serial_UART.h"
#include "OS_ErrorDebouncer.h"
#include "OS_RealTimeClock.h"
#include "OS_Communication.h"

/***************************** defines / macros ******************************/

/************************ local data type definitions ************************/

/************************* local function prototypes *************************/

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/


/****************************** local functions ******************************/


/************************ externally visible functions ***********************/
//***************************************************************************
/*!
\author     KraemerE
\date       30.04.2021
\brief      Entry state of this state.
\return     u8
\param      eEventID - Event which shall be handled while in this state.
\param      uiParam1 - Event parameter from the received event
\param      ulParam2 - Second event parameter from the received event
******************************************************************************/
u8 OS_State_Reset_Entry(teEventID eEventID, uiEventParam1 uiParam1, ulEventParam2 ulParam2)
{
    /* No events expected in this state */
    u8 ucReturn = EVT_PROCESSED;

    /* Make compiler happy */
    (void)uiParam1;
    (void)ulParam2;
    
    switch(eEventID)
    {
        case eEvtEnterResetState:
        {
            /* Initialize software timer */
            bool bTimerInit = OS_SW_Timer_Init();

            /* Initialize UART Module */
            #if USE_OS_SERIAL_UART
            OS_Serial_UART_Init();
            #endif

            /* Initialize Error debouncer */
            OS_ErrorDebouncer_Initialize();

            /* Initialize RTC */
            #if USE_OS_REAL_TIME_CLOCK
            bool bRtcInit = OS_RealTimeClock_Init();
            #endif

            /* Create necessary software timer */
            OS_EVT_PostEvent(eEvtSoftwareTimerCreate, eSwTimer_CreatePeriodic, SW_TIMER_51MS);
            OS_EVT_PostEvent(eEvtSoftwareTimerCreate, eSwTimer_CreatePeriodic, SW_TIMER_251MS);
            OS_EVT_PostEvent(eEvtSoftwareTimerCreate, eSwTimer_CreatePeriodic, SW_TIMER_1001MS);

            /* Switch state to reset state */
            OS_StateManager_CurrentStateReached();
            
            if(bTimerInit == false)
            {
                OS_Communication_SendDebugMessage("SwTimerInit");
            }
            
            #if USE_OS_REAL_TIME_CLOCK
            //Send debug message when RTC init has failed
            if(bRtcInit == false)
            {
                OS_Communication_SendDebugMessage("RtcInit");
            }
            #endif
            break;
        }

        default:
            ucReturn = EVT_NOT_PROCESSED;
            break;
    }

    return ucReturn;
}


//***************************************************************************
/*!
\author     KraemerE
\date       30.04.2021
\brief      Entry state of this state.
\return     u8
\param      eEventID - Event which shall be handled while in this state.
\param      uiParam1 - Event parameter from the received event
\param      ulParam2 - Second event parameter from the received event
******************************************************************************/
u8 OS_State_Reset_Root(teEventID eEventID, uiEventParam1 uiParam1, ulEventParam2 ulParam2)
{
    u8 ucReturn = EVT_NOT_PROCESSED;
    
    /* Make compiler happy */
    (void)eEventID;
    (void)uiParam1;
    (void)ulParam2;
        
    /* Set reached state */
    OS_StateManager_CurrentStateReached();
    
    /* Request next state when available */
    OS_StateManager_RequestNextState();

    return ucReturn;
}


//***************************************************************************
/*!
\author     KraemerE
\date       30.04.2021
\brief      Entry state of this state.
\return     u8
\param      eEventID - Event which shall be handled while in this state.
\param      uiParam1 - Event parameter from the received event
\param      ulParam2 - Second event parameter from the received event
******************************************************************************/
u8 OS_State_Reset_Exit(teEventID eEventID, uiEventParam1 uiParam1, ulEventParam2 ulParam2)
{
    u8 ucReturn = EVT_NOT_PROCESSED;
    
    /* Make compiler happy */
    (void)eEventID;
    (void)uiParam1;
    (void)ulParam2;
    
    OS_StateManager_CurrentStateReached();

    return ucReturn;
}


